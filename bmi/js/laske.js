$(function() {
    $('#laske').click(function() {
        const pituus = parseFloat($('#pituus').val());
        const paino = parseInt($('#paino').val());
        const bmi = paino / Math.pow(pituus,2);
        $('#bmi').val(bmi.toFixed(1));
    });
});